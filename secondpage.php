<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' href='./secondpage.css'>
    <title>Document</title>
</head>
<body>
    <?php
    if ($_SERVER['REQUEST_METHOD'] == "POST"){
        // $_COOKIE = $_POST;
                $q6 = $_POST['name6'];
                $q7 = $_POST['name7'];
                $q8 = $_POST['name8'];
                $q9 = $_POST['name9'];
                $q10 = $_POST['name10'];

          if (isset($_POST['btn2_back'])) 
                {  
                    setcookie("name6",$q6);
                    setcookie("name7",$q7);
                    setcookie("name8",$q8);
                    setcookie("name9",$q9);
                    setcookie("name10",$q10);

                    header('location: firstpage.php');
                }

            if (isset($_POST['btn2_next'])) 
                {  
                    setcookie("name6",$q6);
                    setcookie("name7",$q7);
                    setcookie("name8",$q8);
                    setcookie("name9",$q9);
                    setcookie("name10",$q10);

                    header('location: result.php');
                }
                
        }
            // $count2 = 0;
        ?>
<div class="container_margin">
            <h2 class="text-center">Trang 2</h2>
            <form method='POST'>
                <?php
                    $question6 =    array(
                                        array(
                                            'text' => 'Câu hỏi 6: Hãy chọn A_6', 
                                            'result' => -1
                                        ),
                                        array(
                                            'text' => 'Đáp án A_6', 
                                            'result' => 1
                                        ),
                                        
                                        array(
                                            'text' => 'Đáp án B_6', 
                                            'result' => 0
                                        ),
                                        array(
                                            'text' => 'Đáp án C_6', 
                                            'result' => 0
                                        ),
                                        array(
                                            'text' => 'Đáp án D_6', 
                                            'result' => 0
                                        ),
                                    );  

                    foreach ($question6 as $key => $question)
                    {
                        if($question['result'] == -1){
                            echo "<p>".$question['text']."</p>";
                        }
                        else{
                            echo "<input type='radio' name='name6' value='".$question['result']."' ";
                            echo "/>".$question['text'];
                            echo "</br>";                        
                        }
                        
                    };
                    
                    $question7 =    array(
                        array(
                            'text' => 'Câu hỏi 7: Hãy chọn B_7', 
                            'result' => -1
                        ),
                        array(
                            'text' => 'Đáp án A_7', 
                            'result' => 0
                        ),
                        
                        array(
                            'text' => 'Đáp án B_7', 
                            'result' => 1
                        ),
                        array(
                            'text' => 'Đáp án C_7', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án D_7', 
                            'result' => 0
                        ),
                    );  

                    foreach ($question7 as $key => $question)
                    {
                        if($question['result'] == -1){
                            echo "<p>".$question['text']."</p>";
                        }
                        else{
                            echo "<input type='radio' name='name7' value='".$question['result']."' ";
                            echo "/>".$question['text'];
                            echo "</br>";                        
                        }
                        
                    };

                    $question8 =    array(
                        array(
                            'text' => 'Câu hỏi 8: Hãy chọn C_8', 
                            'result' => -1
                        ),
                        array(
                            'text' => 'Đáp án A_8', 
                            'result' => 0
                        ),
                        
                        array(
                            'text' => 'Đáp án B_8', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án C_8', 
                            'result' => 1
                        ),
                        array(
                            'text' => 'Đáp án D_8', 
                            'result' => 0
                        ),
                    );  

                    foreach ($question8 as $key => $question)
                    {
                        if($question['result'] == -1){
                            echo "<p>".$question['text']."</p>";
                        }
                        else{
                            echo "<input type='radio' name='name8' value='".$question['result']."' ";
                            echo "/>".$question['text'];
                            echo "</br>";                        
                        }
                        
                    };

                    $question9 =    array(
                        array(
                            'text' => 'Câu hỏi 9: Hãy chọn D_9', 
                            'result' => -1
                        ),
                        array(
                            'text' => 'Đáp án A_9', 
                            'result' => 0
                        ),
                        
                        array(
                            'text' => 'Đáp án B_9', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án C_9', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án D_9', 
                            'result' => 1
                        ),
                    );  

                    foreach ($question9 as $key => $question)
                    {
                        if($question['result'] == -1){
                            echo "<p>".$question['text']."</p>";
                        }
                        else{
                            echo "<input type='radio' name='name9' value='".$question['result']."' ";
                            echo "/>".$question['text'];
                            echo "</br>";                        
                        }
                        
                    };

                    $question10 =    array(
                        array(
                            'text' => 'Câu hỏi 10: Hãy chọn C_10', 
                            'result' => -1
                        ),
                        array(
                            'text' => 'Đáp án A_10', 
                            'result' => 0
                        ),
                        
                        array(
                            'text' => 'Đáp án B_10', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án C_10', 
                            'result' => 1
                        ),
                        array(
                            'text' => 'Đáp án D_10', 
                            'result' => 0
                        ),
                    );  

                    foreach ($question10 as $key => $question)
                    {
                        if($question['result'] == -1){
                            echo "<p>".$question['text']."</p>";
                        }
                        else{
                            echo "<input type='radio' name='name10' value='".$question['result']."' ";
                            echo "/>".$question['text'];
                            echo "</br>";                        
                        }
                        
                    };

                ?>
                <br>
                <input type='submit' value='Quay lại' name='btn2_back'></input>
                <input type='submit' value='Nộp bài' name='btn2_next'></input>
            </form>


        </div>
</body>
</html>